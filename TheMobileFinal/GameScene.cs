﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using System.Drawing;
using SpriteKit;
using UIKit;

namespace TheMobileFinal
{
	public class GameScene : SKScene
	{
		public zones[,] Tilearray = new zones[26, 22];

		protected GameScene(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void DidMoveToView(SKView view)
		{
			// Setup your scene here
			var Background = new SKSpriteNode("Map1")
			{
				Position = new CGPoint((Frame.Width / 2), Frame.Height - 244)
			};
			AddChild(Background);
		}

		public void Settilearray(zones[,] ary)
		{
			Tilearray = ary;
			foreach (var zone in Tilearray)
			{
				// Setup your scene here
				var lbltile = new SKSpriteNode("box")
				{
					Position = new CGPoint(zone.Getrect().Left, zone.Getrect().Bottom)
				};
				AddChild(lbltile);



			}
		}

		public override void TouchesBegan(NSSet touches, UIEvent evt)
		{
			// Called when a touch begins
			foreach (var touch in touches)
			{
				Rectangle rect = new Rectangle();
				var location = ((UITouch)touch).LocationInNode(this);
				rect.X = (int)location.X;
				rect.Y = (int)location.Y;
				Console.WriteLine("The Y value is " + location.Y);
				Console.WriteLine("The X value is " + location.X);
				foreach (var zone in Tilearray)
				{
					if (location.X >= zone.Getrect().Left && location.X < zone.Getrect().Right && location.Y >= zone.Getrect().Top && location.Y < zone.Getrect().Bottom)
					{
						Console.WriteLine("This tile is a " + zone.gettiletype() + " Tile.");
						Console.WriteLine(Frame.Width);
						// Setup your scene here
						var lbltile = new SKSpriteNode("trooper")
						{
							Position = new CGPoint(zone.Getrect().X, zone.Getrect().Bottom)
						};
						AddChild(lbltile);
						return;
					}
				}

			}
		}
		public override void Update(double currentTime)
		{
			// Called before each frame is rendered
		}
	}
}
