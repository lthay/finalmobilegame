﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using System.Drawing;
using SpriteKit;
using UIKit;

namespace TheMobileFinal
{
	public partial class GameViewController : UIViewController
	{
		public zones[,] Tilearray = new zones[26, 22];

		protected GameViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			//the text file of the entire layout
			string text = "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwLLLLLLLLLLLLwwwwwwwwwwwwLLLLLLLLLLLLLLLLwwwwwwwwwLLLLLLLLLLLLLLLLMMwwwwwwwwLRRRRRRRRRRRRLLLMMwwwwwwwLLRLLLLLLLLLLLMMLLMMwwwwwwLLRLLLLwwwwLLMMMLLLLwwwwwLLLRLLwwwwwwwwwMMMLLLwwwwwLLLRLLwwwwwwwwwwMLLLLwwwwwLLLRLLwwwLLLLwwwLLLLLwwwwwLLLRLwwwLLLLLLLLLLLLLwwwwwLLLRLwwwLLLLLLLLLLLLLwwwwwLLLRLwwwLLLRRRLLLLLLLwwwwwLLLRLLwwwLLLLRLLLLLLLwwwwwLLLRLLwwwLLLLRLLLLLLLwwwwwLLLRLLwwwwwwLRLLLLLLwwwwwwLLLRLwwwwwwwLRLLLLLwwwwwwwwLLRLwwLLLLLLRLLLLwwwwwwwwwLLRRRRRRLLRRRLLwwwwwwwwwwwLLLLwwLLLLLLLwwwwwwwwwwwwwwLLwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww";
			int count = 0;
			//create the grid for the game u scrub
			for (int i = 0; i < 22; i++)
			{
				for (int j = 0; j < 26; j++)
				{
					//if (text.Substring(count,1) == )) {count+=4;}
					Tilearray[j, i] = new zones(j, -i, text.Substring(count, 1), "unclaimed");
					count++;
					//Tilearray[j, i] = text.Substring(i * 26 + j, 1);
				}
			}
			Rectangle rect = new Rectangle();
			foreach (var zone in Tilearray)
			{
				rect = zone.Getrect();
				rect.X = rect.X + 237;
				rect.Y = rect.Y + 733;
				zone.setrect(rect);
			}
			// Configure the view.
			var skView = (SKView)View;
			skView.ShowsFPS = true;
			skView.ShowsNodeCount = true;
			/* Sprite Kit applies additional optimizations to improve rendering performance */
			skView.IgnoresSiblingOrder = true;

			// Create and configure the scene.
			var scene = SKNode.FromFile<GameScene>("GameScene");
			scene.ScaleMode = SKSceneScaleMode.AspectFill;
			scene.Settilearray(Tilearray);
			// Present the scene.
			skView.PresentScene(scene);

		}
		public override bool ShouldAutorotate()
		{
			return false;
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
		{
			return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone ? UIInterfaceOrientationMask.AllButUpsideDown : UIInterfaceOrientationMask.All;
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public override bool PrefersStatusBarHidden()
		{
			return true;
		}


	}


}
