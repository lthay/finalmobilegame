﻿using System;
using System.Drawing;
using CoreGraphics;
using Foundation;
using SpriteKit;
using UIKit;

namespace TheMobileFinal
{
	public class baseunit : SKScene
	{
		protected Rectangle rect = new Rectangle();
		bool moving;
		//variables-basic health meter
		protected int Hp = 300;
		//shared on the map variable
			//protected int Speed;
		//how many tiles it can move during its turn
		protected int movesleftinturn = 3;
		//a variable to set if the unit can still do anything
		protected bool unitmovementintheturn = true;
		//a variable to tell if the current unit is being clicked
		protected bool Amiclicked = false;
		//shared damage variable
		protected int Dmg = 150;
		//the zone x of the unit
		protected int zonex;
		//the zone y of the unit
		protected int zoney;
		//returning methods, the click method  
		//returnt he rectangle of the class
		public Rectangle getrect()
		{
			return rect;
		}
		//get the freshturns variable
		public Boolean getunitmovementintheturn()
		{
			return unitmovementintheturn;
		}
		//chnge the freshturn variable
		public void changeunitmovementintheturn(bool x)
		{
			unitmovementintheturn = x;
		}
		//change the zones
		public void changezones(int x, int y)
		{
			zonex = x;
			zoney = y;
		}
		//returning the zone digits
		public int getzones(int whichzone)
		{
			if (whichzone == 1)
			{
				return zonex;
			}
			else
			{
				return zoney;
			}
		}
		//get moves left inturn
		public int Getmovesleftinturn()
		{
			return movesleftinturn;
		}
		//change the moves left in the turn variable
		public void Changemovesleftinturn(int x)
		{
			movesleftinturn -= x;
			if (movesleftinturn <= 0)
			{
				movesleftinturn = 0;
			}
		}
		//return the amiclicked 
		public Boolean getamiclicked()
		{
			return Amiclicked;
		}
		//set amiclicked
		public void setamiclicked(bool x)
		{
			Amiclicked = x;
		}
		//attacking 
		public int Gethp()
		{
			return Hp;
		}
		public void ChangeHp(int x)
		{
			Hp -= x;
		}
		public int getdmg()
		{
			return Dmg;
		}
		public void chngemoving(bool x)
		{
			moving = x;
		}
		public Boolean getmoving()
		{
			return moving;
		}
		//useful methods
		//setamiclicked to true or false
		public void setamiclicked()
		{
			if (Amiclicked == true)
			{
				Amiclicked = false;
			}
			else if (Amiclicked == false)
			{
				Amiclicked = true;
			}
		}
		//moving the unit
		public void Move(int x, int y)
		{
			rect.X = x;
			rect.Y = y;
		}
		//constructor-sets the place where the unit is constructed
		public baseunit(int x, int y)
		{
			rect.Height = 16;
			rect.Width = 16;
			rect.X = x;
			rect.Y = y;
		}
	}
}
