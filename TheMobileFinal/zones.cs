﻿using System;
using System.Drawing;

namespace TheMobileFinal
{
	public class zones
	{
		protected Rectangle rect = new Rectangle();
		protected string ownership;
		protected int size = 22;

		//telling what type of terrain it is
		string tiletype = "";
		public string gettiletype()
		{
			return tiletype;
		}
		public string getownership()
		{
			return ownership;
		}
		public void changeownership(string o)
		{
			ownership = o;
		}
		public Rectangle Getrect()
		{
			return rect;
		}
		public void setrect(Rectangle r)
		{
			rect = r;

		}
		public zones(int x, int y, string t, string o)
		{
			rect.Width = size;
			rect.Height = size;
			rect.X = x * size;
			rect.Y = y * size;
			tiletype = t;
			ownership = o;
		}
	}
}
